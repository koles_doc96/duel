package ru.kolesnikov.evgen.connection;

import java.sql.Connection;
import java.sql.DriverManager;

import static ru.kolesnikov.evgen.Constant.*;


public class DBconnection {
private Connection connection;
    private DBconnection() {
        try{
            Class.forName(MYSQL_DRIVER);
            connection =  DriverManager.getConnection(URL, USERNAME, PASSWORD);
            System.out.println("Connection to duel successful!");
        }
        catch(Exception ex){
            System.out.println("Connection failed...");
            System.out.println(ex);
        }
    }

    private static class MysqlConnection {
        private static DBconnection instance;
    }

    public static Connection getInstance(){
        if(MysqlConnection.instance == null) {
            return new DBconnection().connection;
        }
        return MysqlConnection.instance.connection;
    }

}
