package ru.kolesnikov.evgen;

public class Constant {
    public static final String MYSQL_DRIVER = "com.mysql.cj.jdbc.Driver";
    public static final String PASSWORD = "";
    public static final String USERNAME = "root";
    public static final String PLAYER = "player";
    public static final String URL = "jdbc:mysql://localhost:3306/duel?serverTimezone=UTC";
    public static final long PER_SECOND = 1000000 ;


    private Constant() {
    }

    public class UrlConst {

        public static final String ROOT_URL = "/";
        public static final String GAME_SCENA = "/WEB-INF/views/gameScena.jsp";
        public static final String WINNER = "/WEB-INF/views/winner.jsp";

        private UrlConst() {
        }
    }
    public class    DBlayerConstant {
        public static final String SIGNATURE = "HFKDY566HGGB889dbs===";
        public static final String NAME = "name";
        public static final String PASSWORD = "password";
        public static final String ID = "id";
        public static final String DAMAGE = "damage";
        public static final String HEALTH = "health";
        public static final String RATING = "rating";
        public static final int DAMAGE_VALUE = 10;
        public static final int HEALTH_VALUE = 100;
        public static final int RATING_VALUE = 0;
        public static final int READY = 0;
        public static final String PLAYER_ID = "player_id";
        public static final String TIME = "time";
        public static final String TITLE = "title";
        public static final String URL = "url";
        public static final String NAME_OPPONENT = "name_op";
        public static final String RATING_OPPONENT = "rating_op";
        public static final String DAMAGE_OPPONENT = "damage_op";
        public static final String HEALTH_OPPONENT = "health_op";
        public static final String NOT_OPPONENT = "not_op";
        public static final String OPPONENT = "opponent";
        public static final String START = "start";
        public static final String REQ = "req";
        public static final String DB_TIME = "db";
        public static final String DAMAGE_TO = "damage_to";
        public static final String HEALTH_STOCK = "stock";
        public static final String HEALTH_STOCK_OP = "stock_op";

        private DBlayerConstant() {
        }
    }
}
