package ru.kolesnikov.evgen;

public class DataBaseException extends Exception {
    public DataBaseException(String message) {
        super(message);
    }
}
