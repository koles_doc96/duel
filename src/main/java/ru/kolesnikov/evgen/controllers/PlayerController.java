package ru.kolesnikov.evgen.controllers;

import com.mysql.cj.util.TimeUtil;
import ru.kolesnikov.evgen.Constant.DBlayerConstant;
import ru.kolesnikov.evgen.Constant.UrlConst;
import ru.kolesnikov.evgen.DBLayer.DoPlayer;
import ru.kolesnikov.evgen.DataBaseException;
import ru.kolesnikov.evgen.entities.Player;
import ru.kolesnikov.evgen.validators.StringValidator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static ru.kolesnikov.evgen.Constant.DBlayerConstant.PLAYER_ID;
import static ru.kolesnikov.evgen.Constant.PER_SECOND;
import static ru.kolesnikov.evgen.Constant.PLAYER;


@WebServlet(name = "PlayerController", urlPatterns = "/processplayer")
public class PlayerController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        session.invalidate();
        req.getRequestDispatcher(UrlConst.ROOT_URL).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        long start = TimeUtil.getCurrentTimeNanosOrMillis();
        String name = request.getParameter(DBlayerConstant.NAME);
        String password = request.getParameter(DBlayerConstant.PASSWORD);

        List<String> violations = validate(name, password);
        HttpSession session = request.getSession();
        session.setAttribute("damage","");
        Player player = null;
        try {
            player = getPlayer(request.getParameter(DBlayerConstant.NAME), request.getParameter(DBlayerConstant.PASSWORD));
        } catch (DataBaseException e) {
            System.out.println(e.getMessage());
        }
        if(player == null)
        {
            violations.add("invalid password");
        } else {
            setAsRequestAttributes(request, player,
                    (TimeUtil.getCurrentTimeNanosOrMillis() - start) / PER_SECOND);

            session.setAttribute(PLAYER, player);
        }
        if (!violations.isEmpty()) {
            request.setAttribute("violations", violations);
        }

        String url = determineUrl(violations);
        forwardResponse(url, request, response);
    }

    private String determineUrl(List<String> violations) {
        if (!violations.isEmpty()) {
            return UrlConst.ROOT_URL;
        } else {
            return UrlConst.GAME_SCENA;
        }
    }

    private void forwardResponse(String url, HttpServletRequest request, HttpServletResponse response) {
        try {
            request.getRequestDispatcher(url).forward(request, response);
        } catch (ServletException | IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public void setAsRequestAttributes(HttpServletRequest request, Player player, long time) {
        request.setAttribute(DBlayerConstant.NAME, player.getName());
        request.setAttribute(DBlayerConstant.RATING, player.getRating());
        request.setAttribute(DBlayerConstant.DAMAGE, player.getDamage());
        request.setAttribute(DBlayerConstant.HEALTH, player.getHealth());
        request.setAttribute(DBlayerConstant.TIME, time);
        request.setAttribute(DBlayerConstant.REQ, DoPlayer.getCount());
        request.setAttribute(DBlayerConstant.DB_TIME, DoPlayer.getTime());
        request.setAttribute(DBlayerConstant.TITLE, "Main page");
        request.setAttribute(DBlayerConstant.URL, "duel");
    }

    public List<String> validate(String name, String password) {

        List<String> violations = new ArrayList<>();
        if (!StringValidator.validate(name)) {
            violations.add("Name is mandatory");
        }
        if (!StringValidator.validate(password)) {
            violations.add("Password is mandatory");
        }
        return violations;
    }

    public Player getPlayer(String name, String password) throws DataBaseException {
        return DoPlayer.getPlayer(name, password);
    }

}
