package ru.kolesnikov.evgen.controllers;

import com.mysql.cj.util.TimeUtil;
import ru.kolesnikov.evgen.Constant;
import ru.kolesnikov.evgen.DBLayer.DoPlayer;
import ru.kolesnikov.evgen.DataBaseException;
import ru.kolesnikov.evgen.entities.Player;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import  ru.kolesnikov.evgen.Constant.DBlayerConstant;
import static ru.kolesnikov.evgen.Constant.*;

@WebServlet(name = "DuelController", urlPatterns = "/duel")
public class DuelController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        long start = TimeUtil.getCurrentTimeNanosOrMillis();
        HttpSession session = request.getSession();
        Player player = (Player) session.getAttribute(PLAYER);
        Player opponent = null;
        try {
            opponent = DoPlayer.findOpponent(player.getId());
            DoPlayer.ready(player.getId());
        } catch (DataBaseException e) {
            System.out.println(e.getMessage());
        }
        session.setAttribute(DBlayerConstant.OPPONENT,opponent);

        setAsRequestAttributes(request, player, opponent, (TimeUtil.getCurrentTimeNanosOrMillis() - start) / PER_SECOND);
        request.getRequestDispatcher(Constant.UrlConst.GAME_SCENA).forward(request, resp);
    }

    public void setAsRequestAttributes(HttpServletRequest request, Player player, Player playerOpponent, long time) {
        if (playerOpponent != null) {
            request.setAttribute(DBlayerConstant.NAME_OPPONENT, playerOpponent.getName());
            request.setAttribute(DBlayerConstant.RATING_OPPONENT, playerOpponent.getRating());
            request.setAttribute(DBlayerConstant.DAMAGE_OPPONENT, playerOpponent.getDamage());
            request.setAttribute(DBlayerConstant.HEALTH_OPPONENT, playerOpponent.getHealth());
        } else {
            request.setAttribute(DBlayerConstant.NAME_OPPONENT, "Not found opponent");
            request.setAttribute(DBlayerConstant.NOT_OPPONENT, "");

        }

        request.setAttribute(DBlayerConstant.NAME, player.getName());
        request.setAttribute(DBlayerConstant.RATING, player.getRating());
        request.setAttribute(DBlayerConstant.DAMAGE, player.getDamage());
        request.setAttribute(DBlayerConstant.HEALTH, player.getHealth());
        request.setAttribute(DBlayerConstant.TIME, time);
        request.setAttribute(DBlayerConstant.REQ, DoPlayer.getCount());
        request.setAttribute(DBlayerConstant.DB_TIME, DoPlayer.getTime());
        request.setAttribute("duels", "duels");
        request.setAttribute(DBlayerConstant.TITLE, "Duel page");
        request.setAttribute(DBlayerConstant.URL, "startDuel");
    }
}
