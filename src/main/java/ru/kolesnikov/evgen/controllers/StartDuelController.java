package ru.kolesnikov.evgen.controllers;

import com.mysql.cj.util.TimeUtil;
import ru.kolesnikov.evgen.Constant;
import ru.kolesnikov.evgen.DBLayer.DoPlayer;
import ru.kolesnikov.evgen.DataBaseException;
import ru.kolesnikov.evgen.entities.Player;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import ru.kolesnikov.evgen.Constant.DBlayerConstant;
import ru.kolesnikov.evgen.Constant.UrlConst;

import static ru.kolesnikov.evgen.Constant.PER_SECOND;
import static ru.kolesnikov.evgen.Constant.PLAYER;

@WebServlet(name = "StartDuelController", urlPatterns = "/startDuel")
public class StartDuelController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        long start = TimeUtil.getCurrentTimeNanosOrMillis();
        HttpSession session = request.getSession();
        Player player = (Player) session.getAttribute(PLAYER);
        Player opponent = (Player) session.getAttribute(DBlayerConstant.OPPONENT);
        session.setAttribute("healthPlayer", player.getHealth());
        session.setAttribute("healthOp", opponent.getHealth());
        setAsRequestAttributes(request, player, opponent, (TimeUtil.getCurrentTimeNanosOrMillis() - start) / PER_SECOND);
        request.getRequestDispatcher(UrlConst.GAME_SCENA).forward(request, resp);

    }

    public void setAsRequestAttributes(HttpServletRequest request, Player player, Player playerOpponent, long time) {
        setAsRequstAttr(request, player, playerOpponent, time);
    }

    static void setAsRequstAttr(HttpServletRequest request, Player player, Player playerOpponent, long time) {
        request.setAttribute(DBlayerConstant.NAME_OPPONENT, playerOpponent.getName());
        request.setAttribute(DBlayerConstant.RATING_OPPONENT, playerOpponent.getRating());
        request.setAttribute(DBlayerConstant.DAMAGE_OPPONENT, playerOpponent.getDamage());
        request.setAttribute(DBlayerConstant.HEALTH_OPPONENT, playerOpponent.getHealth());
        request.setAttribute(DBlayerConstant.NAME, player.getName());
        request.setAttribute(DBlayerConstant.RATING, player.getRating());
        request.setAttribute(DBlayerConstant.DAMAGE, player.getDamage());
        request.setAttribute(DBlayerConstant.HEALTH, player.getHealth());
        request.setAttribute(DBlayerConstant.REQ, DoPlayer.getCount());
        request.setAttribute(DBlayerConstant.DB_TIME, DoPlayer.getTime());
        request.setAttribute(DBlayerConstant.TIME, time);
        request.setAttribute("duels", "duels");
        request.setAttribute(DBlayerConstant.TITLE, "Duel start");
        request.setAttribute(DBlayerConstant.START, "");
    }
}
