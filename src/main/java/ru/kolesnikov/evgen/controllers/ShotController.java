package ru.kolesnikov.evgen.controllers;

import com.mysql.cj.util.TimeUtil;
import ru.kolesnikov.evgen.Constant;
import ru.kolesnikov.evgen.DBLayer.DoPlayer;
import ru.kolesnikov.evgen.DataBaseException;
import ru.kolesnikov.evgen.entities.Player;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static ru.kolesnikov.evgen.Constant.PER_SECOND;
import static ru.kolesnikov.evgen.Constant.PLAYER;

@WebServlet(name = "ShotController", urlPatterns = "/shot")
public class ShotController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        long start = TimeUtil.getCurrentTimeNanosOrMillis();
        HttpSession session = request.getSession();
        Player player = (Player) session.getAttribute(PLAYER);
        Player opponent = (Player) session.getAttribute(Constant.DBlayerConstant.OPPONENT);
        try {
            opponent = DoPlayer.getById(opponent.getId());
            player = DoPlayer.getById(player.getId());
        } catch (DataBaseException e) {
            System.out.println(e.getMessage());
        }
        int opHealth = Integer.parseInt(session.getAttribute("healthOp").toString());
        int playerHealth = Integer.parseInt(session.getAttribute("healthPlayer").toString());
        session.setAttribute("damage", session.getAttribute("damage").toString() + "\n Player " + opponent.getName() + "Take damage " + player.getDamage());
        if (player.getHealth() <= 0) {

            try {
//                DoPlayer.levelUp(player.getId(), -1, playerHealth);
//                DoPlayer.levelUp(opponent.getId(), 1, opHealth);
                DoPlayer.update(opponent.getId(), opponent.getHealth());
                opponent = DoPlayer.getById(opponent.getId());
                player = DoPlayer.getById(player.getId());
            } catch (DataBaseException e) {
                System.out.println(e.getMessage());
            }
            setAsRequestAttributes(request, opponent, (TimeUtil.getCurrentTimeNanosOrMillis() - start) / PER_SECOND);
            request.getRequestDispatcher(Constant.UrlConst.WINNER).forward(request, resp);

        }
        if (opponent.getHealth() - player.getDamage() > 0) {
            opponent.setHealth(opponent.getHealth() - player.getDamage());
            try {
                DoPlayer.update(opponent.getId(), opponent.getHealth());
                player = DoPlayer.getById(player.getId());
                opponent = DoPlayer.getById(opponent.getId());
            } catch (DataBaseException e) {
                System.out.println(e.getMessage());
            }
            setAsRequestAttributes(request, player, opponent, (TimeUtil.getCurrentTimeNanosOrMillis() - start) / PER_SECOND, session.getAttribute("damage").toString(), playerHealth, opHealth);
            request.getRequestDispatcher(Constant.UrlConst.GAME_SCENA).forward(request, resp);
        } else {
            try {
                opponent.setHealth(opponent.getHealth() - player.getDamage());
                DoPlayer.update(opponent.getId(), opponent.getHealth());
//                DoPlayer.levelUp(player.getId(), 1, playerHealth);
//                DoPlayer.levelUp(opponent.getId(), -1, opHealth);
                player = DoPlayer.getById(player.getId());
            } catch (DataBaseException e) {
                System.out.println(e.getMessage());
            }
            assert player != null;
            setAsRequestAttributes(request, player, (TimeUtil.getCurrentTimeNanosOrMillis() - start) / PER_SECOND);
            request.getRequestDispatcher(Constant.UrlConst.WINNER).forward(request, resp);
        }
    }

    public void setAsRequestAttributes(HttpServletRequest request, Player player, Player playerOpponent, long time, String damage, int health, int opHealth) {
        StartDuelController.setAsRequstAttr(request, player, playerOpponent, time);
        request.setAttribute(Constant.DBlayerConstant.DAMAGE_TO, damage);
        request.setAttribute(Constant.DBlayerConstant.HEALTH_STOCK, health);
        request.setAttribute(Constant.DBlayerConstant.HEALTH_STOCK_OP, opHealth);
    }

    public void setAsRequestAttributes(HttpServletRequest request, Player player, long time) {
        request.setAttribute(Constant.DBlayerConstant.NAME, player.getName());
        request.setAttribute(Constant.DBlayerConstant.RATING, player.getRating());
        request.setAttribute(Constant.DBlayerConstant.DAMAGE, player.getDamage());
        request.setAttribute(Constant.DBlayerConstant.HEALTH, player.getHealth());
        request.setAttribute(Constant.DBlayerConstant.TIME, time);
        request.setAttribute(Constant.DBlayerConstant.REQ, DoPlayer.getCount());
        request.setAttribute(Constant.DBlayerConstant.DB_TIME, DoPlayer.getTime());

    }
}
