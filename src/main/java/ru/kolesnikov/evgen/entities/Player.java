package ru.kolesnikov.evgen.entities;

import lombok.Data;

@Data
public class Player {

    private Integer id;
    private String name;
    private String password;
    private Integer damage;
    private Integer health;
    private Integer rating;

    public Player(String firstName, String lastName) {
        this.name = firstName;
        this.password = lastName;
    }

    public Player(Integer id, String name, Integer damage, Integer health, Integer rating) {
        this.id = id;
        this.name = name;
        this.damage = damage;
        this.health = health;
        this.rating = rating;
    }
}
