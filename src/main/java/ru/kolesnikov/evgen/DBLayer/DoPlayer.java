package ru.kolesnikov.evgen.DBLayer;

import com.mysql.cj.util.TimeUtil;
import org.apache.commons.codec.digest.DigestUtils;
import ru.kolesnikov.evgen.Constant.DBlayerConstant;
import ru.kolesnikov.evgen.DataBaseException;
import ru.kolesnikov.evgen.connection.DBconnection;
import ru.kolesnikov.evgen.entities.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static ru.kolesnikov.evgen.Constant.PER_SECOND;

public class DoPlayer {
    private static int count = 0;
    private static long time = 0;

    public static Player getPlayer(String name, String password) throws DataBaseException {
        try (Connection connection = DBconnection.getInstance()) {
            long startTime = TimeUtil.getCurrentTimeNanosOrMillis();
            try (PreparedStatement statement = connection.prepareStatement("" +
                    "select\n" +
                    "       p.player_id as id," +
                    "       p.name as name,\n" +
                    "       p.password as password,\n" +
                    "       p.damage as damage,\n" +
                    "       p.health as health,\n" +
                    "       p.rating as rating\n" +
                    "from player p\n" +
                    "where p.name = ?")) {
                statement.setString(1, name);
                try (ResultSet resultSet = statement.executeQuery()) {
                    count++;
                    time += (TimeUtil.getCurrentTimeNanosOrMillis() - startTime) / PER_SECOND;
                    if (resultSet.next()) {
                        String pass = resultSet.getString(DBlayerConstant.PASSWORD);
                        if (getPassword(password).equals(pass)) {
                            return new Player(
                                    resultSet.getInt(DBlayerConstant.ID),
                                    resultSet.getString(DBlayerConstant.NAME),
                                    resultSet.getInt(DBlayerConstant.DAMAGE),
                                    resultSet.getInt(DBlayerConstant.HEALTH),
                                    resultSet.getInt(DBlayerConstant.RATING)
                            );
                        }
                        return null;
                    }
                }
            }

            try (PreparedStatement statement = connection.prepareStatement("" +
                    "insert into player values(0,?,?,?,?,?,?);", new String[]{DBlayerConstant.PLAYER_ID})) {
                statement.setString(1, name);
                statement.setString(2, getPassword(password));
                statement.setInt(3, DBlayerConstant.DAMAGE_VALUE);
                statement.setInt(4, DBlayerConstant.HEALTH_VALUE);
                statement.setInt(5, DBlayerConstant.RATING_VALUE);
                statement.setInt(6, DBlayerConstant.READY);

                statement.executeUpdate();
                count++;
                time = (TimeUtil.getCurrentTimeNanosOrMillis() - startTime) / PER_SECOND;
                try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return new Player(generatedKeys.getInt(1), name, DBlayerConstant.DAMAGE_VALUE, DBlayerConstant.HEALTH_VALUE, DBlayerConstant.RATING_VALUE);
                    }
                    throw new DataBaseException("Creating user failed, no ID obtained");
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new DataBaseException(e.getMessage());
        }
    }

    public static String getPassword(String str) {
        return DigestUtils.md5Hex(DigestUtils.sha1Hex(DBlayerConstant.SIGNATURE) + str + DBlayerConstant.SIGNATURE);
    }

    public static Player findOpponent(int id) throws DataBaseException {
        try (Connection connection = DBconnection.getInstance()) {
            long startTime = TimeUtil.getCurrentTimeNanosOrMillis();
            try (PreparedStatement statement = connection.prepareStatement("" +
                    "select\n" +
                    "       p.player_id as id," +
                    "       p.name as name,\n" +
                    "       p.password as password,\n" +
                    "       p.damage as damage,\n" +
                    "       p.health as health,\n" +
                    "       p.rating as rating\n" +
                    "from player p\n" +
                    "where  p.player_id != ? and p.ready = 1"
            )) {
                statement.setInt(1, id);
                try (ResultSet resultSet = statement.executeQuery()) {
                    count++;
                    time += (TimeUtil.getCurrentTimeNanosOrMillis() - startTime) / PER_SECOND;
                    if (resultSet.next()) {
                        return new Player(
                                resultSet.getInt(DBlayerConstant.ID),
                                resultSet.getString(DBlayerConstant.NAME),
                                resultSet.getInt(DBlayerConstant.DAMAGE),
                                resultSet.getInt(DBlayerConstant.HEALTH),
                                resultSet.getInt(DBlayerConstant.RATING));
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new DataBaseException(e.getMessage());
        }
        return null;
    }

    public static void ready(int id) throws DataBaseException {
        try (Connection connection = DBconnection.getInstance()) {
            long startTime = TimeUtil.getCurrentTimeNanosOrMillis();
            try (PreparedStatement statement = connection.prepareStatement("" +
                    "update player set ready = 1\n" +
                    "where  player_id=?")) {
                statement.setInt(1, id);
                statement.executeUpdate();
                count++;
                time += (TimeUtil.getCurrentTimeNanosOrMillis() - startTime) / PER_SECOND;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new DataBaseException(e.getMessage());
        }
    }

    public static void levelUp(int id, int rating, int health) throws DataBaseException {
        try (Connection connection = DBconnection.getInstance()) {
            long startTime = TimeUtil.getCurrentTimeNanosOrMillis();
            try (PreparedStatement statement = connection.prepareStatement("" +
                    "UPDATE player\n" +
                    "SET damage = damage + 1,\n" +
                    "    health = ?,\n" +
                    "    rating = rating + ?,\n" +
                    "    ready = 0\n" +
                    "where player_id =?")) {
                statement.setInt(1, health);
                statement.setInt(2, rating);
                statement.setInt(3, id);
                statement.executeUpdate();
                count++;
                time += (TimeUtil.getCurrentTimeNanosOrMillis() - startTime) / PER_SECOND;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new DataBaseException(e.getMessage());
        }
    }
    public static void update(int id, int health) throws DataBaseException {
        try (Connection connection = DBconnection.getInstance()) {
            long startTime = TimeUtil.getCurrentTimeNanosOrMillis();
            try (PreparedStatement statement = connection.prepareStatement("" +
                    "UPDATE player\n" +
                    "SET \n" +
                    "    health = ?\n" +
                    "where player_id =?")) {
                statement.setInt(1, health);
                statement.setInt(2, id);
                statement.executeUpdate();
                count++;
                time += (TimeUtil.getCurrentTimeNanosOrMillis() - startTime) / PER_SECOND;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new DataBaseException(e.getMessage());
        }
    }

    public static Player getById(int id) throws DataBaseException {
        try (Connection connection = DBconnection.getInstance()) {
            long startTime = TimeUtil.getCurrentTimeNanosOrMillis();
            try (PreparedStatement statement = connection.prepareStatement("" +
                    "select\n" +
                    "       p.player_id as id," +
                    "       p.name as name,\n" +
                    "       p.damage as damage,\n" +
                    "       p.health as health,\n" +
                    "       p.rating as rating\n" +
                    "from player p\n" +
                    "where p.player_id = ?")) {
                statement.setInt(1, id);
                try (ResultSet resultSet = statement.executeQuery()) {
                    count++;
                    time += (TimeUtil.getCurrentTimeNanosOrMillis() - startTime) / PER_SECOND;
                    if (resultSet.next()) {
                            return new Player(
                                    resultSet.getInt(DBlayerConstant.ID),
                                    resultSet.getString(DBlayerConstant.NAME),
                                    resultSet.getInt(DBlayerConstant.DAMAGE),
                                    resultSet.getInt(DBlayerConstant.HEALTH),
                                    resultSet.getInt(DBlayerConstant.RATING)
                            );
                    }
                    return null;
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new DataBaseException(e.getMessage());
        }
    }
    public static int getCount() {
        int copy = count;
        count = 0;
        return copy;
    }

    public static long getTime() {
        long copy = time;
        time = 0;
        return copy;
    }
}
