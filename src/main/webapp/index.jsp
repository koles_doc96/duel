<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign Up</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>
<c:if test="${violations != null}">
    <c:forEach items="${violations}" var="violation">
        <p>${violation}.</p>
    </c:forEach>
</c:if>
<div class="container">
    <div class="row">
        <div class="col align-center offset-4">
            <form action="${pageContext.request.contextPath}/processplayer" method="post">
                <div class="form-group">
                    <label for="name"> Player Name </label>
                    <input class="form-control"  type="text" name="name" id="name" value="${name}" placeholder="Player name">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input class="form-control"  type="password" name="password" id="password" value="${password}" placeholder="Password">
                </div>
                <div class="text-center">
                    <input class="btn btn-primary" type="submit" name="signup" value="Sign Up">
                </div>
            </form>
        </div>
        <div class="col align-center offset-3">
    </div>
</div>
</body>
</html>