<%--
  Created by IntelliJ IDEA.
  User: Evgen
  Date: 09.04.2019
  Time: 12:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Winner</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>
<div class="container">
    <h1 class="text-center">Winner ${name}</h1>
    <div class="row align-center">
        <div class="col align-center offset-1">
            <div class="card align-center" style="width: 18rem;">
                <%--        <img class="card-img-top" src="..." alt="Card image cap">--%>
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <p class="card-text"></p>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><b>Damage: </b> ${damage}</li>
                    <li class="list-group-item">
                        <b>Health: </b> ${health}
                    </li>
                    <li class="list-group-item">
                        <b>Rating: </b> ${rating}
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="align-center">
        <form action="${pageContext.request.contextPath}/duel" method="get">
            <div class="text-center">
                <input class="btn btn-danger" type="submit" name="execute" value="Duel">
            </div>
        </form>
    </div>
</div>
</body>
</html>
