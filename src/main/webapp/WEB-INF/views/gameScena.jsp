<%@ page import="com.mysql.cj.util.TimeUtil" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Duels</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>
<div class="container">
    <h1 class="text-center">${title}</h1>
    <div class="align-right">
        <form action="${pageContext.request.contextPath}/processplayer" method="get">
            <div class="text-center">
                <input class="btn btn-info" type="submit" name="singout" value="singout">
            </div>
        </form>
    </div>
    <div class="row align-center">
        <div class="col align-left offset-1">
            <div class="card align-center" style="width: 18rem;">
                <%--        <img class="card-img-top" src="..." alt="Card image cap">--%>
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <p class="card-text"></p>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><b>Damage: </b> ${damage}</li>
                    <li class="list-group-item">
                        <b>Health: </b> ${health}
                        <c:if test="${start !=null}">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: ${health}%" aria-valuenow="${health}" aria-valuemin="0"
                                     aria-valuemax="${stock}"></div>
                            </div>
                        </c:if>
                    </li>
                    <li class="list-group-item">
                        <b>Rating: </b> ${rating}
                    </li>
                </ul>
                <c:if test="${start != null}">
                    <div class="card-body">
                        <a href="${pageContext.request.contextPath}/shot" class="card-link">Shot</a>
                    </div>
                </c:if>
            </div>
        </div>
        <c:if test="${duels != null}">
            <div class="col align-right offset-2">
                <div class="card align-center" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title">${name_op}</h5>
                        <p class="card-text"></p>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><b>Damage: </b> ${damage_op}</li>
                        <li class="list-group-item">
                            <b>Health: </b> ${health_op}
                            <c:if test="${start !=null}">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: ${health_op}%" aria-valuenow="${health_op}" aria-valuemin="0"
                                         aria-valuemax="${stock_op}"></div>
                                </div>
                            </c:if>
                        </li>
                        <li class="list-group-item">
                            <b>Rating: </b> ${rating_op}
                        </li>
                    </ul>
                </div>
            </div>
        </c:if>

    </div>
    <c:if test="${not_op == null && start ==null}">
        <div class="align-center">
            <form action="${pageContext.request.contextPath}/${url}" method="get">
                <div class="text-center">
                    <input class="btn btn-danger" type="submit" name="execute" value="${url}">
                </div>
            </form>
        </div>
    </c:if>
    <c:if test="${not_op != null}">
        <div class="align-center">
            <form action="${pageContext.request.contextPath}/duel" method="get">
                <div class="text-center">
                    <input class="btn btn-danger" type="submit" name="execute" value="Duel">
                </div>
            </form>
        </div>
    </c:if>
</div>
<div class="container">
    <pre>${damage_to}</pre>
</div>
<div class="container">
    <h3>page:${time} ms, db: ${req}req (${db}ms)</h3>
</div>
</body>
</html>